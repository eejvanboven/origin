package ss.week1;

public class Lamp {

	public static final int OFF = 0 ;
	
	public static final int LOW = 1 ;
	
	public static final int MEDIUM = 2 ;
	
	public static final int HIGH = 3 ;
	
	private int setting;
	
	public Lamp(){
		setting = OFF;
	}
	public int setting(){
		return setting;
	}
	
	public void change() {
		setting = (setting +1) %4;
	}
	
}
